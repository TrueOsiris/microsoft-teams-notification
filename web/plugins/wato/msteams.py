#!/usr/bin/python3

# link        https://www.zibris.ch
# copyright   Zibris AG
# author      Michael Kleger <m.kleger@zibris.ch>

# -*- encoding: utf-8; py-indent-offset: 4 -*-

register_notification_parameters("msteams", Dictionary(
    elements = [
        ("WebhookUrl", TextAscii(
            title = _("Webhook Url"),
            help = _("Microsoft Teams Webhook Url"),
            allow_empty = False,
        )),
        ("WebhookRetries", TextAscii(
            title = _("Webhook retries"),
            help = _("Microsoft Teams Webhook retries (Default: 20)"),
            size = 2,
            allow_empty = True,
        )),
    ]
))